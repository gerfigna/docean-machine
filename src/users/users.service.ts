import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async setHasuraCheck(userId: string){
    let userEntity = await this.usersRepository.findOne(userId)
    userEntity.hasuraCheck = true;
    return await this.usersRepository.save(userEntity);
  }

  async create(user: User): Promise<User> {
    let userEntity = await this.usersRepository.create(user);

    return this.usersRepository.save(userEntity);
  }

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(id: string): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }
}
