import { Controller, Get, HttpStatus, Post, Req, Res } from '@nestjs/common';
import { UsersService } from './users.service';
import { Request, Response } from 'express';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Req() req: Request, @Res() res: Response) {
    const user = req.body;

    this.usersService
      .create(user)
      .then(() => res.status(HttpStatus.OK).json({ msg: 'created' }));
  }
  @Get()
  async findAll(@Res() res: Response) {
    res.status(HttpStatus.OK).json(await this.usersService.findAll());
  }

  // @Get()
  // async faindAll(): Promise<string> {
  //     return JSON.stringify( await this.usersService.findAll());
  //
  //
  // }
}
