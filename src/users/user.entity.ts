import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  age: number;

  @Column({ default: true })
  isActive: boolean;

  @Column({ nullable: true })
  createdAt: Date;

  @Column({ default: false })
  hasuraCheck: boolean;

  @BeforeInsert()
  setCreatedAt() {
    this.createdAt = new Date();
  }
}
