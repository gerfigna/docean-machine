import { Module } from '@nestjs/common';
import { UserSubscriber } from './user.subscriber';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'USERS_SERVICE_POOL',
        transport: Transport.RMQ,
        options: {
          urls: ['amqp://192.168.99.100:5672'],
          queue: 'users',
        },
      },
    ]),
  ],
  providers: [UserSubscriber],
  exports: [UserSubscriber],
})
export class SubscriberModule {}
