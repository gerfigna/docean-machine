import {
  Connection,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm';
import { User } from '../users/user.entity';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<User> {
  constructor(
    private dbConn: Connection,
    @Inject('USERS_SERVICE') private queueClient: ClientProxy,
  ) {
    dbConn.subscribers.push(this);
  }

  listenTo() {
    return User;
  }

  afterInsert(event: InsertEvent<User>) {
    console.log(`BEFORE USER INSERTED: `, event.entity);

    this.queueClient.emit<number>(
      'user_created',
        event.entity
    );
  }
}
