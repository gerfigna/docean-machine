import { Injectable } from '@nestjs/common';
import { HasuraEventHandler, HasuraEvent } from '@golevelup/nestjs-hasura';
import {UsersService} from "../users/users.service";

@Injectable()
export class Userhandler {
    constructor(private userService: UsersService) {
    }

    @HasuraEventHandler({
        table: { name: 'user' },
    })
    async handleUserCreated(evt: HasuraEvent) {
        let data = <any>evt.event.data;
        console.log(evt.event.data)
        await this.userService.setHasuraCheck(data.new.id)
    }
}
