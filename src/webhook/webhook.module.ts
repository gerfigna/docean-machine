import { Module } from '@nestjs/common';
import { HasuraModule } from '@golevelup/nestjs-hasura';
import { Userhandler } from './userhandler';
import {UsersModule} from "../users/users.module";

@Module({
    imports: [
        UsersModule,
        HasuraModule.forRoot(HasuraModule, {
            secretFactory: 'secret',
            secretHeader: 'api-secret-header',
            // controllerPrefix: 'something', // this is optional. defaults to hasura
        }),
    ],
    providers: [Userhandler],
})
export class WebhookModule {}
