FROM registry.gitlab.com/gerfigna/nestjs-docker-base:latest as base

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

#PRODUCTION
FROM base as prod
ENV NODE_ENV=production

RUN npm install hasura-cli

COPY ./ .
RUN npm run build

CMD [ "nodemon" ]

#DEV
FROM base as dev
ENV NODE_ENV=development

CMD ["npm", "run", "start:dev"]
